import {Injectable} from '@nestjs/common';
import {IScheduleItem} from "./types";
import axios from "axios";
import * as cheerio from "cheerio";
import {CourseSeason, EducationLevel} from "../../common/types";

@Injectable()
export class ScheduleService {
    async getScheduleData(year: number, season: string): Promise<IScheduleItem[]> {
        let schedules: IScheduleItem[] = [];
        let numberForSeason: number;
        let seasonName: CourseSeason;

        switch (season) {
            case 'summer':
                numberForSeason = 3
                seasonName = CourseSeason.SUMMER;
                year--;
                break;
            case 'spring':
                numberForSeason = 2;
                seasonName = CourseSeason.SPRING;
                year--;
                break;
            default://Осінь
                numberForSeason = 1;
                seasonName = CourseSeason.AUTUMN;
        }

        const currentResponse = await axios.get(`https://my.ukma.edu.ua/schedule/?year=${year}&season=${numberForSeason}`);
        const $ = cheerio.load(currentResponse.data);

        //bring information to normal state
        $('#schedule-accordion > div').each((i, panel) => {
            const curFaculty = $(panel).find("div").eq(0).find("a").text().trim();
            const facultyInformation = $(panel).find("div").eq(1).find("div[id^='schedule-faculty'] > div");
            let levelText = $(panel).find('div > h4 > a').first().text().split(', ')[0].trim();

            facultyInformation.each((id, el) => {
                //bachelor or master schedule
                const bachOrMas = $(el).find("a").text().trim().split(", ");
                const curYear: number = +bachOrMas[1].split(" ")[0];

                const allInfoAboutSchedule = $(el).find("div[id^='schedule-faculty'] li > div");
                allInfoAboutSchedule.each((index, element) => {
                    let curURL = String($(element).find('div > a').last().attr('href'));
                    let curSpecialityName = $(element).find('div > a').last().text().trim().split(levelText)[0].trim();
                    const [, date, time] = $(element).find('span').text().trim().split(' ');

                    schedules.push({
                        url: curURL,
                        updatedAt: `${date} ${time}`,
                        facultyName: curFaculty,
                        specialityName: curSpecialityName,
                        level: bachOrMas[0] === 'БП' ? EducationLevel.BACHELOR : EducationLevel.MASTER,
                        year: (curYear == 1) ? 1: (curYear == 2) ? 2: (curYear == 3) ? 3: 4,
                        season: seasonName
                    });

                });
            });
        });
        return schedules;
    }
}
