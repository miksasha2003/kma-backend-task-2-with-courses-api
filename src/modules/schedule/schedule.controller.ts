import {Controller, Param, Get, BadRequestException, InternalServerErrorException, NotFoundException } from '@nestjs/common';
import { ScheduleService } from './schedule.service';

@Controller('schedule')
export class ScheduleController {
  constructor(

      protected readonly service: ScheduleService,
  ) {}

  @Get(':year/:season')
  public async getScheduleData(@Param('year') year: number, @Param('season') season: string): Promise<unknown> {

    require('https').globalAgent.options.rejectUnauthorized = false;

    return this.service.getScheduleData(year, season).catch(error => {

      switch (error?.response?.status) {
        case 404://error is here. Why?
          throw new NotFoundException('404 error. Not found');
        case 400:
          throw new BadRequestException('HTTP 400 Bad Request');
        default:
          throw new InternalServerErrorException('Unknown error');
      }

    });
  }
}
