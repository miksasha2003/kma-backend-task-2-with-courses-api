import {Controller, Param, Get, BadRequestException, InternalServerErrorException, NotFoundException } from '@nestjs/common';
import { CourseService } from './course.service';

@Controller('course')
export class CourseController {
  constructor(
      protected readonly service: CourseService,
  ) {}

  @Get(':code')
  public async getCourseData(@Param('code') courseCode: number): Promise<unknown> {

    require('https').globalAgent.options.rejectUnauthorized = false;

    return this.service.getInfoAboutCourse(courseCode).catch(error => {

      switch (error?.response?.status) {
        case 404:
          throw new NotFoundException('404 error. Not found');
        case 400:
          throw new BadRequestException('HTTP 400 Bad Request');
        default:
          throw new InternalServerErrorException('Unknown error');
      }

    });
  }
}
