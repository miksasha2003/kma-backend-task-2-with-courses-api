import {Injectable} from '@nestjs/common';
import { ICourse } from './types';
import * as cheerio from 'cheerio';
import {CourseSeason, EducationLevel} from '../../common/types';
import axios from 'axios';

@Injectable()
export class CourseService {
    async getInfoAboutCourse(courseCode: number): Promise<ICourse> {
        const currentResponse = await axios.get(`https://my.ukma.edu.ua/course/${courseCode}`);

        const $ = cheerio.load(currentResponse.data);

        let curCode = Number($('#w0 > table > tbody:nth-child(1) > tr:nth-child(1) > td').text());
        let curName = ($('div.container > ul > li:nth-child(3)')).text();
        let curDescription = ($('#course-card--' + courseCode + '--info').text().replace(/[\n\t]/g, ''));
        let curFacultyName = ($('#w0 > table > tbody:nth-child(1) > tr:nth-child(3) > td').text());
        let curDepartmentName = ($('#w0 > table > tbody:nth-child(1) > tr:nth-child(4) > td').text());
        let curLevelText = ($('#w0 > table > tbody:nth-child(1) > tr:nth-child(5) > td').text());
        let curLevel = (curLevelText === 'Бакалавр') ? EducationLevel.BACHELOR: EducationLevel.MASTER;
        let curYear = Number($('#w0 > table > tbody:nth-child(1) > tr:nth-child(2) > td > span:nth-child(3)').text().split(' ')[0]);

        let trimester: CourseSeason[] = []
        $('#w0 > table > tbody:nth-child(2)').each((_number, element) => {
            //еш аштв trimester
            const temp_trimester = $(element).find("th").text().split('\t');
            temp_trimester.forEach(t => {
                switch (t) {
                    case 'Літо':
                        trimester.push(CourseSeason.SUMMER);
                        break;
                    case 'Весна':
                        trimester.push(CourseSeason.SPRING);
                        break;
                    default://Осінь
                        trimester.push(CourseSeason.AUTUMN);
                }
            });
        });

        let curCreditsAmount = Number($('#w0 > table > tbody:nth-child(1) > tr:nth-child(2) > td > span:nth-child(1)').text().split(' ')[0]);
        let curHoursAmount = Number($('#w0 > table > tbody:nth-child(1) > tr:nth-child(2) > td > span:nth-child(2)').text().split(' ')[0]);
        let curTeacherName = ($('#w0 > table > tbody:nth-child(1) > tr:nth-child(7) > th').next().text());

        return {
            code: curCode,
            name: curName,
            description: curDescription,
            facultyName: curFacultyName,
            departmentName: curDepartmentName,
            level: curLevel,
            year: (curYear == 1) ? 1: (curYear == 2) ? 2: (curYear == 3) ? 3: 4,
            seasons: trimester,
            creditsAmount: curCreditsAmount,
            hoursAmount: curHoursAmount,
            teacherName: curTeacherName
        };
    }
}
